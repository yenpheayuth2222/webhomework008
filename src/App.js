import './App.css';
import NavMen from './Components/NavMen';
import Contents from './Components/Contents';
import React, { Component } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import DataTable from './Components/DataTable';


class App extends Component {

  constructor() {
    super();
    this.state = {
        data : [{
                id: 0,
                username: "",
                gender: "",
                email: "",
                password: "", 
                isSelected: true,
            }],
            newId : 1
        }

        this.onGetData = this.onGetData.bind(this)
        this.onDelete = this.onDelete.bind(this)
        this.onGetSelect = this.onGetSelect.bind(this)
   }

  onGetData(data){
    let addData = { 
                    id : this.state.data.length,
                    username : data.username,
                    gender : data.gender,
                    email : data.email, 
                    password : data.password, 
                    isSelected : false
                  }
    let newData = [...this.state.data, addData]
    this.setState({
      data : newData
    })
  }

  onDelete(){
    let afterDel = this.state.data.filter(item =>{
      return item.isSelected !== true
    })

    this.setState({
      data : afterDel
    })
  }

  onGetSelect(id){
    let temp = this.state.data.filter(item =>{
      return item.id > 0
    })
    let data = [...temp]
    data[id].isSelected = !data[id].isSelected
    this.setState({
      data : data
    })
  }

  render() {
    return (
      <Container>
        <NavMen/>
        <Row className="m-4">
          <Col sm="4">
            <Contents
              onGetData = {this.onGetData}
            />
          </Col>
          <Col sm="8">
            <DataTable 
              items={this.state.data}
              onGetSelect = {this.onGetSelect}
              onDelete = {this.onDelete}        
              />
          </Col>
        </Row>
      </Container>
    )
  }
}
export default App;
