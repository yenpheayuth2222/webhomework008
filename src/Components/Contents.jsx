import React, { Component } from 'react'
import {Button,Form,Image} from "react-bootstrap";

export default class Contents extends Component {

    constructor(props) {
        super(props);
        this.state = {

            };
      }
    onDataChange = (e) =>{
        this.setState(
            {
              [e.target.name]: e.target.value
            })
    }

    render() {
        return (
            <div>
            <Form>
                <label htmlFor="myfile">
                <Image
                    className="mx-5"
                    width="170px"
                    height="150px"
                    src="profile.png"
                    roundedCircle
                />
                </label>

                <input
                    onChange={this.onUploadFile}
                    style={{ display: "none" }}
                    id="myfile"
                    type="file"
                />

                <h3 className="h3 my-1">Create Account</h3>

                <Form.Group controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Username" name="username" onChange={this.onDataChange}/>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Gender</Form.Label><br/>
                    <Form.Check
                        custom
                        inline
                        label="Male"
                        type="radio"
                        id="male"
                        value="Male"
                        name="gender"
                        onChange={this.onDataChange}
                        defaultChecked={true}
                    />
                    <Form.Check
                        custom
                        inline
                        label="Female"
                        type="radio"
                        id="female"
                        value="Female"
                        name="gender"
                        onChange={this.onDataChange}
                    />
                </Form.Group>

                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" name="email" onChange={this.onDataChange}/>
                    
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" name="password" onChange={this.onDataChange}/>
                </Form.Group>

                <Button 
                    variant="primary" 
                    type="button" 
                    size="sm"
                    onClick={()=>{
                        this.props.onGetData(this.state)
                    }}>Save     
                </Button>
            </Form> 
        </div>
        )
    }
}

